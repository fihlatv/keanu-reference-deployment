output "matrix_alb_log_prefix" {
  value = "${local.matrix_alb_log_prefix}"
}

output "log_bucket_id" {
  value = "${module.log_bucket.bucket_id}"
}

output "log_bucket_arn" {
  value = "${module.log_bucket.bucket_arn}"
}
