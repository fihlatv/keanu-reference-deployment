terraform {
  required_version = ">= 0.11.2"
  backend          "s3"             {}
}

provider "aws" {
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

data "sops_file" "common" {
  source_file = "../_data/common.enc.yaml"
}

locals {
  region      = "${data.sops_file.common.data.region}"
  name        = "${data.sops_file.common.data.name}"
  environment = "${data.sops_file.common.data.environment}"
  namespace   = "${data.sops_file.common.data.namespace}"
}

data "aws_elb_service_account" "main" {}

module "log_bucket" {
  source          = "../../../../keanu-terraform-modules/log-bucket"
  enabled         = "true"
  namespace       = "${local.namespace}"
  environment     = "${local.environment}"
  name            = "${local.name}"
  attributes      = ["logs"]
  expiration_days = 90

  ## TODO dev setting
  force_destroy = true
}

### PUT ALL LOG POLICIES HERE FOR ALL THE SERVICES
locals {
  matrix_alb_log_prefix = "alb/matrix-stack"
}

data "aws_iam_policy_document" "matrix_alb" {
  statement = [{
    sid       = "AllowMatrixALBToPutLoadBalancerLogsToS3Bucket"
    actions   = ["s3:PutObject"]
    resources = ["${module.log_bucket.bucket_arn}/${local.matrix_alb_log_prefix}/*"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_elb_service_account.main.id}:root"]
    }
  }]
}

data "aws_iam_policy_document" "combined" {
  statement = ["${flatten(data.aws_iam_policy_document.matrix_alb.*.statement)}"]
}

resource "aws_s3_bucket_policy" "combined" {
  bucket = "${module.log_bucket.bucket_id}"
  policy = "${data.aws_iam_policy_document.combined.json}"
}
