output "acme_account_production_account_key_pem" {
  value     = "${data.sops_file.acme_account.data.key}"
  sensitive = true
}

output "acme_account_staging_account_key_pem" {
  value     = "${data.sops_file.acme_account_staging.data.key}"
  sensitive = true
}

output "acme_account_production_server_url" {
  value = "${data.sops_file.acme_account.data.server_url}"
}

output "acme_account_staging_server_url" {
  value = "${data.sops_file.acme_account_staging.data.server_url}"
}
