terraform {
  required_version = ">= 0.11.2"

  backend "s3" {}
}

provider "sops" {}
provider "tls" {}

data "sops_file" "acme_account" {
  source_file = "../_data/acme-account.enc.yaml"
}

data "sops_file" "acme_account_staging" {
  source_file = "../_data/acme-account-staging.enc.yaml"
}

provider "acme" {
  alias      = "staging"
  server_url = "${data.sops_file.acme_account_staging.data.server_url}"
}

provider "acme" {
  alias      = "production"
  server_url = "${data.sops_file.acme_account.data.server_url}"
}

resource "acme_registration" "reg_staging" {
  account_key_pem = "${data.sops_file.acme_account_staging.data.key}"
  email_address   = "${data.sops_file.acme_account_staging.data.email}"
  provider        = "acme.staging"
}

resource "acme_registration" "reg_production" {
  account_key_pem = "${data.sops_file.acme_account.data.key}"
  email_address   = "${data.sops_file.acme_account.data.email}"
  provider        = "acme.production"
}
