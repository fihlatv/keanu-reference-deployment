output "matrix_certificate_pem" {
  value = "${module.matrix_certs.certificate_pem}"
}

output "matrix_private_key_pem" {
  value     = "${module.matrix_certs.private_key_pem}"
  sensitive = true
}

output "matrix_issuer_pem" {
  value = "${module.matrix_certs.issuer_pem}"
}

output "riot_certificate_pem" {
  value = "${module.riot_certs.certificate_pem}"
}

output "riot_private_key_pem" {
  value     = "${module.riot_certs.private_key_pem}"
  sensitive = true
}

output "riot_issuer_pem" {
  value = "${module.riot_certs.issuer_pem}"
}
