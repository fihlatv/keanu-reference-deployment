terraform {
  required_version = ">= 0.11.2"

  backend "s3" {}
}

provider "aws" {
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

provider "sops" {}
provider "tls" {}

data "sops_file" "common" {
  source_file = "../_data/common.enc.yaml"
}

data "sops_file" "cloudflare" {
  source_file = "../_data/cloudflare.enc.yaml"
}

data "sops_file" "tls_keys" {
  source_file = "../_data/tls-private-keys.enc.yaml"
}

data "sops_file" "tls_keys_staging" {
  source_file = "../_data/tls-private-keys-staging.enc.yaml"
}

provider "cloudflare" {
  email = "${data.sops_file.cloudflare.data.cloudflare_email}"
  token = "${data.sops_file.cloudflare.data.cloudflare_token}"
}

data "terraform_remote_state" "letsencrypt_accounts" {
  backend = "s3"

  config = {
    bucket = "${local.namespace}-${local.environment}-${local.name}-terraform-state"
    key    = "letsencrypt-accounts/terraform.tfstate"
  }
}

provider "acme" {
  alias      = "staging"
  server_url = "${data.terraform_remote_state.letsencrypt_accounts.acme_account_staging_server_url}"
}

provider "acme" {
  alias      = "production"
  server_url = "${data.terraform_remote_state.letsencrypt_accounts.acme_account_production_server_url}"
}

locals {
  namespace          = "${data.sops_file.common.data.namespace}"
  environment        = "${data.sops_file.common.data.environment}"
  name               = "${data.sops_file.common.data.name}"
  matrix_dns_names   = "${split(",", data.sops_file.common.data.matrix_dns_names)}"
  matrix_common_name = "${data.sops_file.common.data.matrix_common_name}"
  riot_dns_names     = "${split(",", data.sops_file.common.data.riot_dns_names)}"
  riot_common_name   = "${data.sops_file.common.data.riot_common_name}"
}

module "matrix_certs" {
  source                       = "../../../../keanu-terraform-modules/letsencrypt-certs"
  use_production               = "1"
  common_name                  = "${local.matrix_common_name}"
  dns_names                    = ["${local.matrix_dns_names}"]
  acme_account_key_pem         = "${data.terraform_remote_state.letsencrypt_accounts.acme_account_production_account_key_pem}"
  acme_account_staging_key_pem = "${data.terraform_remote_state.letsencrypt_accounts.acme_account_staging_account_key_pem}"
  cloudflare_email             = "${data.sops_file.cloudflare.data.cloudflare_email}"
  cloudflare_token             = "${data.sops_file.cloudflare.data.cloudflare_token}"
  tls_private_key              = "${data.sops_file.tls_keys.data.matrix.key}"
  tls_private_key_algo         = "${data.sops_file.tls_keys.data.matrix.algo}"
  tls_staging_private_key      = "${data.sops_file.tls_keys_staging.data.matrix.key}"
  tls_staging_private_key_algo = "${data.sops_file.tls_keys_staging.data.matrix.algo}"
  namespace                    = "${local.namespace}"
  environment                  = "${local.environment}"
  name                         = "${local.name}"
}

module "riot_certs" {
  source                       = "../../../../keanu-terraform-modules/letsencrypt-certs"
  use_production               = "1"
  common_name                  = "${local.riot_common_name}"
  dns_names                    = ["${local.riot_dns_names}"]
  acme_account_key_pem         = "${data.terraform_remote_state.letsencrypt_accounts.acme_account_production_account_key_pem}"
  acme_account_staging_key_pem = "${data.terraform_remote_state.letsencrypt_accounts.acme_account_staging_account_key_pem}"
  cloudflare_email             = "${data.sops_file.cloudflare.data.cloudflare_email}"
  cloudflare_token             = "${data.sops_file.cloudflare.data.cloudflare_token}"
  tls_private_key              = "${data.sops_file.tls_keys.data.riot.key}"
  tls_private_key_algo         = "${data.sops_file.tls_keys.data.riot.algo}"
  tls_staging_private_key      = "${data.sops_file.tls_keys_staging.data.riot.key}"
  tls_staging_private_key_algo = "${data.sops_file.tls_keys_staging.data.riot.algo}"
  namespace                    = "${local.namespace}"
  environment                  = "${local.environment}"
  name                         = "${local.name}"
}
