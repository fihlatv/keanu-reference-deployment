output "region" {
  value = "${module.vpc.region}"
}

output "igw_id" {
  value       = "${module.vpc.igw_id}"
  description = "The ID of the Internet Gateway"
}

output "vpc_id" {
  value       = "${module.vpc.vpc_id}"
  description = "The ID of the VPC"
}

output "vpc_cidr_block" {
  value       = "${module.vpc.vpc_cidr_block}"
  description = "The CIDR block of the VPC"
}

output "vpc_main_route_table_id" {
  value       = "${module.vpc.vpc_main_route_table_id}"
  description = "The ID of the main route table associated with this VPC."
}

output "vpc_default_network_acl_id" {
  value       = "${module.vpc.vpc_default_network_acl_id}"
  description = "The ID of the network ACL created by default on VPC creation"
}

output "vpc_default_security_group_id" {
  value       = "${module.vpc.vpc_default_security_group_id}"
  description = "The ID of the security group created by default on VPC creation"
}

output "vpc_default_route_table_id" {
  value       = "${module.vpc.vpc_default_route_table_id}"
  description = "The ID of the route table created by default on VPC creation"
}

output "vpc_ipv6_association_id" {
  value       = "${module.vpc.vpc_ipv6_association_id}"
  description = "The association ID for the IPv6 CIDR block"
}

output "ipv6_cidr_block" {
  value       = "${module.vpc.ipv6_cidr_block}"
  description = "The IPv6 CIDR block"
}

output "subnet_public_1_id" {
  value = "${module.vpc.subnet_public_1_id}"
}

output "subnet_public_1_cidr_block" {
  value = "${module.vpc.subnet_public_1_cidr_block}"
}

output "subnet_public_1_availability_zone" {
  value = "${module.vpc.subnet_public_1_availability_zone}"
}

output "subnet_public_2_id" {
  value = "${module.vpc.subnet_public_2_id}"
}

output "subnet_public_2_cidr_block" {
  value = "${module.vpc.subnet_public_2_cidr_block}"
}

output "subnet_public_2_availability_zone" {
  value = "${module.vpc.subnet_public_2_availability_zone}"
}

output "subnet_private_1_id" {
  value = "${module.vpc.subnet_private_1_id}"
}

output "subnet_private_1_cidr_block" {
  value = "${module.vpc.subnet_private_1_cidr_block}"
}

output "subnet_private_1_availability_zone" {
  value = "${module.vpc.subnet_private_1_availability_zone}"
}

#output "subnet_private_2_id" {
#  value = "${module.vpc.subnet_private_2_id}"
#}
#output "subnet_private_2_cidr_block" {
#  value = "${module.vpc.subnet_private_2_cidr_block}"
#}
#output "subnet_private_2_availability_zone" {
#  value = "${module.vpc.subnet_private_2_availability_zone}"
#}

output "subnet_rds_a_1_id" {
  value = "${module.vpc.subnet_rds_a_1_id}"
}

output "subnet_rds_a_1_cidr_block" {
  value = "${module.vpc.subnet_rds_a_1_cidr_block}"
}

output "subnet_rds_a_1_availability_zone" {
  value = "${module.vpc.subnet_rds_a_1_availability_zone}"
}

output "subnet_rds_a_2_id" {
  value = "${module.vpc.subnet_rds_a_2_id}"
}

output "subnet_rds_a_2_cidr_block" {
  value = "${module.vpc.subnet_rds_a_2_cidr_block}"
}

output "subnet_rds_a_2_availability_zone" {
  value = "${module.vpc.subnet_rds_a_2_availability_zone}"
}

output "subnet_rds_subnet_group" {
  value = "${module.vpc.subnet_rds_subnet_group}"
}

output "nat_gw_eip_id" {
  value = "${module.vpc.nat_gw_eip_id}"
}

output "nat_gw_eip_public_ip" {
  value = "${module.vpc.nat_gw_eip_public_ip}"
}

output "nat_gw_id" {
  value = "${module.vpc.nat_gw_id}"
}

output "sg_private_ssh_id" {
  value = "${module.vpc.sg_private_ssh_id}"
}
