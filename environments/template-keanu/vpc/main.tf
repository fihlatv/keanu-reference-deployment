terraform {
  required_version = ">= 0.11.2"

  backend "s3" {}
}

provider "aws" {
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

data "sops_file" "common" {
  source_file = "../_data/common.enc.yaml"
}

locals {
  namespace           = "${data.sops_file.common.data.namespace}"
  environment         = "${data.sops_file.common.data.environment}"
  name                = "${data.sops_file.common.data.name}"
  region              = "${data.sops_file.common.data.region}"
  availability_zone_1 = "${data.sops_file.common.data.availability_zone_1}"
  availability_zone_2 = "${data.sops_file.common.data.availability_zone_2}"
  cidr_block          = "${data.sops_file.common.data.cidr_block}"
  subnet_public_1     = "${data.sops_file.common.data.subnet_public_1}"
  subnet_public_2     = "${data.sops_file.common.data.subnet_public_2}"
  subnet_private_1    = "${data.sops_file.common.data.subnet_private_1}"
  subnet_private_2    = "${data.sops_file.common.data.subnet_private_2}"
  subnet_rds_a_1      = "${data.sops_file.common.data.subnet_rds_a_1}"
  subnet_rds_a_2      = "${data.sops_file.common.data.subnet_rds_a_2}"
}

module "vpc" {
  source              = "../../../../keanu-terraform-modules/vpc"
  namespace           = "${local.namespace}"
  environment         = "${local.environment}"
  name                = "${local.name}"
  availability_zone_1 = "${local.availability_zone_1}"
  availability_zone_2 = "${local.availability_zone_2}"
  region              = "${local.region}"
  cidr_block          = "${local.cidr_block}"
  subnet_public_1     = "${local.subnet_public_1}"
  subnet_public_2     = "${local.subnet_public_2}"
  subnet_private_1    = "${local.subnet_private_1}"
  subnet_private_2    = "${local.subnet_private_2}"
  subnet_rds_a_1      = "${local.subnet_rds_a_1}"
  subnet_rds_a_2      = "${local.subnet_rds_a_2}"
}
