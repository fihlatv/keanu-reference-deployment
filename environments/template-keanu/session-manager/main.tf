terraform {
  required_version = ">= 0.11.2"

  backend "s3" {}
}

provider "aws" {
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

provider "sops" {}

data "sops_file" "common" {
  source_file = "../_data/common.enc.yaml"
}

data "sops_file" "bastion" {
  source_file = "../_data/bastion.enc.yaml"
}

data "sops_file" "cloudflare_secrets" {
  source_file = "../_data/cloudflare.enc.yaml"
}

locals {
  namespace   = "${data.sops_file.common.data.namespace}"
  environment = "${data.sops_file.common.data.environment}"
  name        = "${data.sops_file.common.data.name}"
  region      = "${data.sops_file.common.data.region}"
}

module "session_manager" {
  source      = "../../../../keanu-terraform-modules/session-manager-preferences"
  namespace   = "${local.namespace}"
  environment = "${local.environment}"
  name        = "${local.name}"
  region      = "${local.region}"
}
