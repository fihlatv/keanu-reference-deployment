output "s3_bucket_domain_name" {
  value       = "${module.session_manager.s3_bucket_domain_name}"
  description = "S3 bucket domain name"
}

output "s3_bucket_id" {
  value       = "${module.session_manager.s3_bucket_id}"
  description = "S3 bucket ID"
}

output "s3_bucket_arn" {
  value       = "${module.session_manager.s3_bucket_arn}"
  description = "S3 bucket ARN"
}

output "document_name" {
  value       = "${module.session_manager.document_name}"
  description = "Name of the created document."
}

output "document_arn" {
  value       = "${module.session_manager.document_arn}"
  description = "ARN of the created document. You can use this to create IAM policies that prevent changes to session manager preferences."
}
