output "ssh_user" {
  value = "${module.bastion.ssh_user}"
}

output "security_group_id" {
  value = "${module.bastion.security_group_id}"
}

output "asg_id" {
  value = "${module.bastion.asg_id}"
}

output "bastion_host" {
  value = "${module.bastion.bastion_host}"
}

output "bastion_public_ip" {
  value = "${module.bastion.bastion_public_ip}"
}
