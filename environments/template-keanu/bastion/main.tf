terraform {
  required_version = ">= 0.11.2"

  backend "s3" {}
}

provider "aws" {
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

provider "sops" {}

data "sops_file" "common" {
  source_file = "../_data/common.enc.yaml"
}

data "sops_file" "bastion" {
  source_file = "../_data/bastion.enc.yaml"
}

data "sops_file" "cloudflare_secrets" {
  source_file = "../_data/cloudflare.enc.yaml"
}

provider "cloudflare" {
  email = "${data.sops_file.cloudflare_secrets.data.cloudflare_email}"
  token = "${data.sops_file.cloudflare_secrets.data.cloudflare_token}"
}

locals {
  namespace            = "${data.sops_file.common.data.namespace}"
  environment          = "${data.sops_file.common.data.environment}"
  name                 = "${data.sops_file.common.data.name}"
  cloudflare_zone      = "${data.sops_file.cloudflare_secrets.data.cloudflare_zone}"
  bastion_domain_name  = "bastion.${data.sops_file.common.data.matrix_common_name}"
  ssh_public_key_names = "${split(",", data.sops_file.bastion.data.ssh_public_key_names)}"
  public_keys_bucket   = "${local.namespace}-${local.environment}-ssh-keys"
}

data "aws_ami" "debian" {
  most_recent = true

  filter {
    name   = "name"
    values = ["debian-stretch-hvm-x86_64-gp2-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["379101102735"] # Debian
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "${local.namespace}-${local.environment}-${local.name}-terraform-state"
    key    = "vpc/terraform.tfstate"
  }
}

resource "aws_s3_bucket" "ssh_public_keys" {
  region = "${data.terraform_remote_state.vpc.region}"
  bucket = "${local.public_keys_bucket}"
  acl    = "private"
}

resource "aws_s3_bucket_object" "ssh_public_keys" {
  bucket = "${aws_s3_bucket.ssh_public_keys.bucket}"
  key    = "${element(local.ssh_public_key_names, count.index)}.pub"

  # Make sure that you put files into correct location and name them accordingly (`public_keys/{keyname}.pub`)
  source = "../_data/public_keys/${element(local.ssh_public_key_names, count.index)}.pub"
  count  = "${length(local.ssh_public_key_names)}"

  depends_on = ["aws_s3_bucket.ssh_public_keys"]
}

module "bastion" {
  source             = "../../../../keanu-terraform-modules/bastion"
  bastion_ami        = "${data.aws_ami.debian.id}"
  cloudflare_zone    = "${local.cloudflare_zone}"
  domain_name        = "${local.bastion_domain_name}"
  region             = "${data.terraform_remote_state.vpc.region}"
  vpc_id             = "${data.terraform_remote_state.vpc.vpc_id}"
  subnet_ids         = ["${data.terraform_remote_state.vpc.subnet_public_1_id}"]
  public_keys_bucket = "${local.public_keys_bucket}"
  namespace          = "${local.namespace}"
  environment        = "${local.environment}"
  name               = "bastion"
}
