variable "aws_assume_role_arn" {
  type = "string"
}

variable "key_name" {
  type = "string"
}
