output "synapse_ip" {
  value = "${module.matrix_stack.synapse_ip}"
}

output "sygnal_ip" {
  value = "${module.matrix_stack.sygnal_ip}"
}

output "mxisd_ip" {
  value = "${module.matrix_stack.mxisd_ip}"
}

output "certificate_pem" {
  value = "${data.terraform_remote_state.letsencrypt_certs.matrix_certificate_pem}"
}

output "iam_certificate_id" {
  value = "${module.matrix_stack.iam_certificate_id}"
}

output "iam_certificate_arn" {
  value = "${module.matrix_stack.iam_certificate_arn}"
}

output "alb_dns_name" {
  value = "${module.matrix_stack.alb_dns_name}"
}

output "web_client_url" {
  value = "https://${data.sops_file.common.data.riot_common_name}"
}

output "homeserver_url" {
  value = "https://${data.sops_file.common.data.matrix_common_name}"
}

output "identity_server_url" {
  value = "https://${data.sops_file.common.data.matrix_common_name}"
}
