terraform {
  required_version = ">= 0.11.2"
  backend          "s3"             {}
}

provider "sops" {}

data "sops_file" "matrix" {
  source_file = "../_data/matrix-stack.enc.yaml"
}

data "sops_file" "common" {
  source_file = "../_data/common.enc.yaml"
}

locals {
  region      = "${data.sops_file.common.data.region}"
  name        = "${data.sops_file.common.data.name}"
  environment = "${data.sops_file.common.data.environment}"
  namespace   = "${data.sops_file.common.data.namespace}"
}

data "sops_file" "cloudflare" {
  source_file = "../_data/cloudflare.enc.yaml"
}

data "sops_file" "ios_push_settings" {
  source_file = "../_data/ios.enc.yaml"
}

data "sops_file" "android_push_settings" {
  source_file = "../_data/android.enc.yaml"
}

provider "aws" {
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

provider "cloudflare" {
  email = "${data.sops_file.cloudflare.data.cloudflare_email}"
  token = "${data.sops_file.cloudflare.data.cloudflare_token}"
}

data "terraform_remote_state" "log_bucket" {
  backend = "s3"

  config = {
    bucket = "${local.namespace}-${local.environment}-${local.name}-terraform-state"
    key    = "log-bucket/terraform.tfstate"
  }
}

data "terraform_remote_state" "session_manager" {
  backend = "s3"

  config = {
    bucket = "${local.namespace}-${local.environment}-${local.name}-terraform-state"
    key    = "session-manager/terraform.tfstate"
  }
}

data "terraform_remote_state" "letsencrypt_certs" {
  backend = "s3"

  config = {
    bucket = "${local.namespace}-${local.environment}-${local.name}-terraform-state"
    key    = "letsencrypt-certs/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "${local.namespace}-${local.environment}-${local.name}-terraform-state"
    key    = "vpc/terraform.tfstate"
  }
}

data "terraform_remote_state" "rds" {
  backend = "s3"

  config = {
    bucket = "${local.namespace}-${local.environment}-${local.name}-terraform-state"
    key    = "rds/terraform.tfstate"
  }
}

module "matrix_stack" {
  source = "../../../../keanu-terraform-modules/matrix-stack"

  ### GENERAL
  region                    = "${local.region}"
  database_port             = "${data.terraform_remote_state.rds.this_db_instance_port}"
  database_name             = "${data.terraform_remote_state.rds.this_db_instance_name}"
  database_username         = "${data.terraform_remote_state.rds.this_db_instance_username}"
  database_password         = "${data.terraform_remote_state.rds.this_db_instance_password}"
  database_address          = "${data.terraform_remote_state.rds.this_db_instance_address}"
  database_endpoint         = "${data.terraform_remote_state.rds.this_db_instance_endpoint}"
  subnet_id                 = "${data.terraform_remote_state.vpc.subnet_private_1_id}"
  availability_zone_1       = "${data.terraform_remote_state.vpc.subnet_private_1_availability_zone}"
  security_group_id_general = "${data.terraform_remote_state.vpc.sg_private_ssh_id}"
  key_name                  = "${var.key_name}"
  domain_name               = "${data.sops_file.common.data.matrix_common_name}"
  cloudflare_zone           = "${data.sops_file.cloudflare.data.cloudflare_zone}"
  vpc_id                    = "${data.terraform_remote_state.vpc.vpc_id}"
  vpc_cidr_block            = "${data.terraform_remote_state.vpc.vpc_cidr_block}"
  certificate_pem           = "${data.terraform_remote_state.letsencrypt_certs.matrix_certificate_pem}"
  private_key_pem           = "${data.terraform_remote_state.letsencrypt_certs.matrix_private_key_pem}"
  issuer_pem                = "${data.terraform_remote_state.letsencrypt_certs.matrix_issuer_pem}"
  aws_log_bucket            = "${data.terraform_remote_state.log_bucket.log_bucket_id}"
  aws_log_bucket_log_prefix = "${data.terraform_remote_state.log_bucket.matrix_alb_log_prefix}"
  session_manager_bucket    = "${data.terraform_remote_state.session_manager.s3_bucket_domain_name}"

  ## ALB
  # TODO dev setting
  enable_deletion_protection = false

  alb_public_subnets = ["${data.terraform_remote_state.vpc.subnet_public_1_id}", "${data.terraform_remote_state.vpc.subnet_public_2_id}"]

  ### SYNAPSE
  ami_synapse                 = "${data.sops_file.matrix.data.ami_synapse}"
  instance_type_synapse       = "${data.sops_file.matrix.data.instance_type_synapse}"
  web_client_base_url         = "https://${data.sops_file.common.data.riot_common_name}"
  macaroon_secret_key         = "${data.sops_file.matrix.data.macaroon_secret_key}"
  password_config_pepper      = "${data.sops_file.matrix.data.password_config_pepper}"
  shared_registration_secret  = "${data.sops_file.matrix.data.shared_registration_secret}"
  synapse_signing_key         = "${data.sops_file.matrix.data.synapse_signing_key}"
  synapse_disk_allocation_gb  = "${data.sops_file.matrix.data.synapse_disk_allocation_gb}"
  manhole_enabled             = "false"
  federation_enabled          = "true"
  metrics_enabled             = "true"
  federation_domain_whitelist = ["matrix.org"]
  metrics_bind_addresses      = ["127.0.0.1"]
  admin_contact_email         = "${data.sops_file.matrix.data.admin_contact_email}"

  ### SYGNAL
  ami_sygnal           = "${data.sops_file.matrix.data.ami_sygnal}"
  instance_type_sygnal = "${data.sops_file.matrix.data.instance_type_sygnal}"
  sygnal_ios           = "${data.sops_file.ios_push_settings.data}"
  sygnal_android       = "${data.sops_file.android_push_settings.data}"

  ### MXISD
  ami_mxisd            = "${data.sops_file.matrix.data.ami_mxisd}"
  instance_type_mxisd  = "${data.sops_file.matrix.data.instance_type_mxisd}"
  mxisd_hostname       = "${data.sops_file.common.data.matrix_common_name}"
  mxisd_email_host     = "${data.sops_file.matrix.data.mxisd_email_host}"
  mxisd_email_login    = "${data.sops_file.matrix.data.mxisd_email_login}"
  mxisd_email_password = "${data.sops_file.matrix.data.mxisd_email_password}"
  mxisd_email_from     = "${data.sops_file.matrix.data.mxisd_email_from}"
  mxisd_email_port     = "${data.sops_file.matrix.data.mxisd_email_port}"

  ### METADATA
  namespace   = "${local.namespace}"
  environment = "${local.environment}"
  name        = "${local.name}"

  tags = {
    "Project" = "${local.name}"
  }
}
