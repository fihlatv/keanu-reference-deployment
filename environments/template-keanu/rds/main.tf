terraform {
  required_version = ">= 0.11.2"

  backend "s3" {}
}

provider "aws" {
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

provider "sops" {}

data "sops_file" "common" {
  source_file = "../_data/common.enc.yaml"
}

data "sops_file" "rds" {
  source_file = "../_data/synapse-rds.enc.yaml"
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "${local.namespace}-${local.environment}-${local.name}-terraform-state"
    key    = "vpc/terraform.tfstate"
  }
}

locals {
  namespace                = "${data.sops_file.common.data.namespace}"
  environment              = "${data.sops_file.common.data.environment}"
  name                     = "${data.sops_file.common.data.name}"
  rds_instance_class       = "${data.sops_file.rds.data.rds_instance_class}"
  rds_allocated_storage    = "${data.sops_file.rds.data.rds_allocated_storage}"
  rds_engine               = "${data.sops_file.rds.data.rds_engine}"
  rds_engine_version       = "${data.sops_file.rds.data.rds_engine_version}"
  rds_major_engine_version = "${data.sops_file.rds.data.rds_major_engine_version}"
  rds_family               = "${data.sops_file.rds.data.rds_family}"
}

module "db" {
  source = "../../../../keanu-terraform-modules/rds"

  engine                      = "${local.rds_engine}"
  instance_class              = "${local.rds_instance_class}"
  engine_version              = "${local.rds_engine_version}"
  major_engine_version        = "${local.rds_major_engine_version}"
  family                      = "${local.rds_family}"
  allocated_storage           = "${local.rds_allocated_storage}"
  vpc_id                      = "${data.terraform_remote_state.vpc.vpc_id}"
  vpc_cidr_block              = "${data.terraform_remote_state.vpc.vpc_cidr_block}"
  subnet_ids                  = ["${data.terraform_remote_state.vpc.subnet_rds_a_1_id}", "${data.terraform_remote_state.vpc.subnet_rds_a_2_id}"]
  deletion_protection_enabled = false
  skip_final_snapshot         = true
  allow_major_version_upgrade = false
  apply_immediately           = true
  backup_retention_period     = 30
  database_name               = "${local.name}"
  database_username           = "${local.name}"
  database_password           = "${data.sops_file.rds.data.database_password}"
  namespace                   = "${local.namespace}"
  environment                 = "${local.environment}"
  name                        = "${local.name}"
  attributes                  = ["rds"]
}
