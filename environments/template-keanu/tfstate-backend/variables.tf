variable "tf_bucket_region" {
  type        = "string"
  description = "The region where the terraform state bucket is stored"
}
