terraform {
  required_version = ">= 0.11.2"

  # comment the following line out when bootstrapping
  backend "s3" {}
}

# comment the following line out when bootstrapping
variable "aws_assume_role_arn" {}

provider "aws" {
  region = "${var.tf_bucket_region}"

  assume_role {
    # comment the role_arn line out when bootstrapping
#    role_arn = "${var.aws_assume_role_arn}"
  }
}

data "sops_file" "common" {
  source_file = "../_data/common.enc.yaml"
}

locals {
  region      = "${data.sops_file.common.data.region}"
  name        = "${data.sops_file.common.data.name}"
  environment = "${data.sops_file.common.data.environment}"
  namespace   = "${data.sops_file.common.data.namespace}"
}

module "main" {
  source        = "../../../../keanu-terraform-modules/tfstate-backend"
  namespace     = "${local.namespace}"
  environment   = "${local.environment}"
  name          = "${local.name}"
  region        = "${var.tf_bucket_region}"
  force_destroy = "true"
}

output "tfstate_backend_s3_bucket_domain_name" {
  value = "${module.main.tfstate_backend_s3_bucket_domain_name}"
}

output "tfstate_backend_s3_bucket_id" {
  value = "${module.main.tfstate_backend_s3_bucket_id}"
}

output "tfstate_backend_s3_bucket_arn" {
  value = "${module.main.tfstate_backend_s3_bucket_arn}"
}

output "tfstate_backend_dynamodb_table_name" {
  value = "${module.main.tfstate_backend_dynamodb_table_name}"
}

output "tfstate_backend_dynamodb_table_id" {
  value = "${module.main.tfstate_backend_dynamodb_table_id}"
}

output "tfstate_backend_dynamodb_table_arn" {
  value = "${module.main.tfstate_backend_dynamodb_table_arn}"
}
