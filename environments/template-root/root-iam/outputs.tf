output "admin_group" {
  value = "${module.main.admin_group}"
}

output "admin_switchrole_url" {
  description = "URL to the IAM console to switch to the admin role"
  value       = "${module.main.admin_switchrole_url}"
}

output "readonly_group" {
  value = "${module.main.readonly_group}"
}

output "readonly_switchrole_url" {
  description = "URL to the IAM console to switch to the readonly role"
  value       = "${module.main.readonly_switchrole_url}"
}

output "role_admin_arn" {
  description = "Admin role ARN"
  value       = "${module.main.role_admin_arn}"
}

output "role_readonly_arn" {
  description = "Readonly role ARN"
  value       = "${module.main.role_readonly_arn}"
}

output "billing_full_access_group" {
  value = "${module.main.billing_full_access_group}"
}
