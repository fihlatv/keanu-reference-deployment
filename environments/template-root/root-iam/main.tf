terraform {
  required_version = ">= 0.11.2"

  backend "s3" {}
}

provider "aws" {
  assume_role {
    #role_arn = "${var.aws_assume_role_arn}"
  }
}

module "main" {
  source      = "../../../../keanu-terraform-modules/root-iam"
  namespace   = "${var.namespace}"
  environment = "${var.environment}"
  root_account_namespace = "${var.namespace}"
}
