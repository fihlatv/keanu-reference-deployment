variable "production_account_arn" {}
variable "production_account_id" {}

locals {
  production_organization_account_access_role = "arn:aws:iam::${var.production_account_id}:role/OrganizationAccountAccessRole"
}

module "production_account" {
  source                           = "../../../../keanu-terraform-modules/accounts"
  namespace                        = "${var.namespace}"
  environment                      = "production"
  accounts_enabled                 = ["${var.accounts_enabled}"]
  account_id                       = "${var.production_account_id}"
  account_arn                      = "${var.production_account_arn}"
  organization_account_access_role = "${local.production_organization_account_access_role}"
}

output "production_account_arn" {
  value = "${var.production_account_arn}"
}

output "production_account_id" {
  value = "${var.production_account_id}"
}

output "production_organization_account_access_role" {
  value = "${local.production_organization_account_access_role}"
}

output "production_switchrole_url" {
  description = "URL to the IAM console to switch to the production account organization access role"
  value       = "${module.production_account.switchrole_url}"
}
