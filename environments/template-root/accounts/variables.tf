variable "aws_assume_role_arn" {
  type = "string"
}

variable "namespace" {
  type        = "string"
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = "string"
  description = "environment (e.g. `prod`, `dev`, `staging`)"
  default     = "root"
}

variable "name" {
  type        = "string"
  description = "Application or solution name (e.g. `app`)"
  default     = "account"
}

variable "accounts_enabled" {
  type        = "list"
  description = "Accounts to enable"
}
