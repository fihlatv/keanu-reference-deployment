variable "development_account_arn" {}
variable "development_account_id" {}

locals {
  development_organization_account_access_role = "arn:aws:iam::${var.development_account_id}:role/OrganizationAccountAccessRole"
}

module "development_account" {
  source                           = "../../../../keanu-terraform-modules/accounts"
  namespace                        = "${var.namespace}"
  environment                      = "development"
  accounts_enabled                 = ["${var.accounts_enabled}"]
  account_id                       = "${var.development_account_id}"
  account_arn                      = "${var.development_account_arn}"
  organization_account_access_role = "${local.development_organization_account_access_role}"
}

output "development_account_arn" {
  value = "${var.development_account_arn}"
}

output "development_account_id" {
  value = "${var.development_account_id}"
}

output "development_organization_account_access_role" {
  value = "${local.development_organization_account_access_role}"
}

output "development_switchrole_url" {
  description = "URL to the IAM console to switch to the development account organization access role"
  value       = "${module.development_account.switchrole_url}"
}
