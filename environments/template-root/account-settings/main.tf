terraform {
  required_version = ">= 0.11.2"

  backend "s3" {}
}

provider "aws" {
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

module "account_settings" {
  source      = "../../../../keanu-terraform-modules/account-settings"
  namespace   = "${var.namespace}"
  environment = "${var.environment}"
  name        = "${var.name}"
  enabled     = "${var.enabled}"
}
