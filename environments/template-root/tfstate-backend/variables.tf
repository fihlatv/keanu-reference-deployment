# comment the following line out when bootstrapping
variable "aws_assume_role_arn" {}

variable "tf_bucket_region" {
  type        = "string"
  description = "The region where the terraform state bucket is stored"
}

variable "namespace" {
  type        = "string"
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = "string"
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
  default = "root"
}

