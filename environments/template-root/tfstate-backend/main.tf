terraform {
  required_version = ">= 0.11.2"

  # comment the following line out when bootstrapping
  backend "s3" {}
} 
provider "aws" {
  region = "${var.tf_bucket_region}"

  # comment the following block out when bootstrapping
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

module "main" {
  source        = "../../../../keanu-terraform-modules/tfstate-backend"
  namespace     = "${var.namespace}"
  environment   = "${var.environment}"
  region        = "${var.tf_bucket_region}"
  force_destroy = "true"
}
