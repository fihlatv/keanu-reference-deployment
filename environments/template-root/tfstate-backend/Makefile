.EXPORT_ALL_VARIABLES:
TF_ROOT                     ?= $(realpath ../../../../keanu-terraform-modules)
MODULE_ROOT                  = ${TF_ROOT}/tfstate-backend
TF_VAR_aws_assume_role_arn  ?= $(shell aws sts get-caller-identity --output text --query 'Arn' | sed 's/:sts:/:iam:/g' | sed 's,:assumed-role/,:role/,' | cut -d/ -f1-2 | grep role)
.PHONY: init destroy check_destroy force-destroy

## Initialize the terraform state backend
init:
	@${MODULE_ROOT}/$@.sh
	@[ -d .terraform ] || terraform init

## Destroy the configuration (only works if `force_destroy=true`)
destroy: check-destroy
	${MODULE_ROOT}/$@.sh

## Force destroy the bucket (not recommended!)
force-destroy: check-destroy
	${MODULE_ROOT}/$@.sh $(TF_BUCKET) $(TF_DYNAMODB_TABLE)

check-destroy:
	@echo -n "WARNING: You are about to destroy your state bucket, this is impossible to revert.\n\n"
	@( read -p "Do you want to destroy? [y/N]: " sure && case "$$sure" in [yY]) true;; *) false;; esac )

