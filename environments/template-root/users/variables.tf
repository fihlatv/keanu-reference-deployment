variable "aws_assume_role_arn" {
  type = "string"
}

variable "namespace" {
  type = "string"
}

variable "environment" {
  type        = "string"
  description = "Environment (e.g. `production`, `development`, `staging`)"
  default     = "root"
}
