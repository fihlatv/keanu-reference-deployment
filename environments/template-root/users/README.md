## Adding a New User

To add new users, add them to this directory. Add new users by adding a `.tf`
file in this directory for their configuration.

To get started, copy the `user-jane.tf.sample` to `jane.tf`

Note: It is our convention to have the user name be their email address, even
though AWS does not require this.

You can specify the `pgp_key` variable with `keybase:usernameonkeybase` and it
will fetch their PGP automatically. OR you can place the public key in a local
file, and load it that way.

To load the PGP key from a file:

1. Export it gpg 
    ```bash
    gpg --export-options export-minimal --export jane@example.com > ../_data/users/jane.pubkey
    ```
2. Specify the `pgp_key` variable like so
    ```hcl
    pgp_key       = "${base64encode(file("../_data/users/jane.pubkey"))}"
    ```

To apply the new user:

1. `source ../env`
2. `aws-vault exec keanu-root-admin -- make plan`
3. `aws-vault exec keanu-root-admin -- make apply`
4. Continue to the next section.

### Temporary Login Credentials

After provisioning the user, their base64 encrypted password will be
output by the module. A welcome message suitable for copy/pasting into an email
or Signal message will also be printed.

To retrieve a user's base64 encrypted password decrypt command, run the following command:

```
terraform output jane@example.com
```

Continue to the next section.

### User Account Setup

The user will need to then setup their AWS account. They will do this by going
to the [login URL](../../README.md) for the "root" account.

To login, they will need to enter their email address followed by
their temporary password which was obtained by running the decrypt command from
the terraform output in the previous section.

1. After setting up their password, the user will need to [attach an MFA
   device](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa_enable_virtual.html)
   to their account or ask for from from their friendly sysadmin.
2. Then they will need to log out and log back in again using their MFA device.
   Using this new login session, then navigate to the [IAM menu and obtain the
   AWS
   credentials](https://aws.amazon.com/blogs/security/wheres-my-secret-access-key/)
   (*AWS Access Key ID* and *AWS Secret Access Key*) for their account. These
   will be needed when setting up their shell with `aws-vault`.

## Editing/Removing a user

1. `source ../env`
2. Edit/remove their `user`.tf file in this directory.
3. `aws-vault exec keanu-root-admin -- make plan`
4. `aws-vault exec keanu-root-admin -- make apply`


