terraform {
  required_version = ">= 0.11.2"

  backend "s3" {}
}

provider "aws" {
  assume_role {
    role_arn = "${var.aws_assume_role_arn}"
  }
}

data "terraform_remote_state" "account_settings" {
  backend = "s3"

  config {
    bucket = "${var.namespace}-${var.environment}-terraform-state"
    key    = "account-settings/terraform.tfstate"
  }
}

data "terraform_remote_state" "accounts" {
  backend = "s3"

  config {
    bucket = "${var.namespace}-${var.environment}-terraform-state"
    key    = "accounts/terraform.tfstate"
  }
}

locals {
  accounts_enabled = "${concat(list("root"), data.terraform_remote_state.accounts.accounts_enabled)}"
}

# Fetch the OrganizationAccountAccessRole ARNs from SSM
module "admin_groups" {
  source         = "../../../../keanu-terraform-modules/ssm-parameter-store"
  parameter_read = "${formatlist("/${var.namespace}/%s/admin_group", local.accounts_enabled)}"
}

# Fetch the name of the group that allows full access to billing
module "billing_full_access_group" {
  source         = "../../../../keanu-terraform-modules/ssm-parameter-store"
  parameter_read = ["/${var.namespace}/root/billing_full_access_group"]
}

locals {
  account_alias             = "${data.terraform_remote_state.account_settings.account_alias}"
  signin_url                = "${data.terraform_remote_state.account_settings.signin_url}"
  admin_groups              = ["${module.admin_groups.values}"]
  billing_full_access_group = "${module.billing_full_access_group.values}"
  admin_with_billing_groups = ["${module.admin_groups.values}", "${local.billing_full_access_group}"]
}
