# keanu-reference-deployment

Get started with Matrix and Keanu quickly using this reference deployment.

To learn how to use this repository, head to [docs.keanu.im](http://docs.keanu.im).

# License

All code in this repo is licensed under the [GNU Affero General Public License
(AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html), unless other wise
stated.

# Author Information

* Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)

