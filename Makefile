export ROOT_MODULES_REPO   ?= $(realpath ../keanu-terraform-modules)

# -----------------------------------------------------------------------------
ifeq ("$(wildcard $(ROOT_MODULES_REPO))","")
$(error ROOT_MODULES_REPO "$(ROOT_MODULES_REPO)" does not exist. Please consult setup documentation.)
endif

print-repo-path:
	@echo $(ROOT_MODULES_REPO)

